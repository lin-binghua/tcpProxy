
SHARED_DIR = share
NOWTIME=$(shell date +%s)

share-build:
	@$(MAKE) -C $(SHARED_DIR) \
		CC="$(CROSS_COMPILE)gcc" \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		ARCH="$(ARCH)" \
		all
share-install:
	@$(MAKE) -C $(SHARED_DIR) \
		CC="$(CROSS_COMPILE)gcc" \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		ARCH="$(ARCH)" \
		CONFIG_PREFIX="$(TARGETDIR)" \
		install

share-clean:
	@$(MAKE) -C $(SHARED_DIR) clean
	