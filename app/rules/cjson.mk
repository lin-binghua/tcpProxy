
CJSON_DIR = cjson

cjson-build:
	@$(MAKE) -C $(CJSON_DIR) \
		CC="$(CROSS_COMPILE)gcc" \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		ARCH="$(ARCH)" \
		all
cjson-install:
	@$(MAKE) -C $(CJSON_DIR) \
		CC="$(CROSS_COMPILE)gcc" \
		CROSS_COMPILE="$(CROSS_COMPILE)" \
		KBUILD_HAVE_NLS=no \
		EXTRA_CFLAGS="$(TARGET_CFLAGS)" \
		ARCH="$(ARCH)" \
		CONFIG_PREFIX="$(TARGETDIR)" \
		install

cjson-clean:
	@$(MAKE) -C $(CJSON_DIR) clean
	