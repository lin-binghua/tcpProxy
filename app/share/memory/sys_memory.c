
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <ctype.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/stat.h>

#include "private_type.h"
#include "private_log.h"
#include "ipc_protocol.h"
#include "memory/sys_memory.h"

MEM_INFO *g_pstCvnMemInfo = NULL;
void MEM_DelDebugInfo(UINT8 *pBuf);

static UINT8 MEM_IsDebugOn()
{
    if((access(MEM_DEBUG_ON, F_OK)) != -1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}

static MEM_INFO *MEM_DebugShmInit(void)
{
    UINT32 shmid;
    void *pMemAddr = (void *)0;
    shmid = shmget((key_t)MEM_DDEBUG_SHMID, MEM_MAX_NUM * sizeof(MEM_INFO), 0666|IPC_CREAT);
	if(shmid==-1)
    {
        CW_SendErr( "[%s:%d] shmget failed", __FUNCTION__, __LINE__);
        return NULL;
    }
    pMemAddr = (void *)shmat(shmid,(void *)0,0);
	if((void *)-1 == pMemAddr)
	{
	    CW_SendErr( "[%s:%d] shmat failed", __FUNCTION__, __LINE__);
		return NULL;
	}
    return (MEM_INFO *)pMemAddr;
}

static MEM_INFO *MEM_DebugInfoInit(void)
{
    if (NULL == g_pstCvnMemInfo)
    {
        g_pstCvnMemInfo = MEM_DebugShmInit();
        if(NULL == g_pstCvnMemInfo)
        {
            CW_SendErr( "[%s,%d]MEM_DebugShmInit error",
                    __FUNCTION__, __LINE__);
            return NULL;
        }
    }

    return g_pstCvnMemInfo;
}

UINT32 MEM_SetDebug(UINT8 ucDebug)
{
    FILE *fp = NULL;
    MEM_INFO *pstMemInfo = NULL;
    if (TRUE == ucDebug)
    {
        if (TRUE == MEM_IsDebugOn())
        {
            return IPC_STATUS_OK;
        }

        if((fp=fopen(MEM_DEBUG_ON,"wb"))==NULL)
        {
            CW_SendErr(  "[%s,%d]open file error, file=%s",
                    __FUNCTION__, __LINE__, MEM_DEBUG_ON);
            return IPC_STATUS_FAIL;
        }
        fclose(fp);

        if ((pstMemInfo = MEM_DebugInfoInit()) == NULL)
        {
            CW_SendErr(  "[%s,%d] get shm error ", __FUNCTION__, __LINE__);

            return IPC_STATUS_ARGV;
        }

        memset(pstMemInfo, 0, sizeof(MEM_INFO) * MEM_MAX_NUM);
    }
    else
    {
        remove(MEM_DEBUG_ON);
    }
    return IPC_STATUS_OK;
}

UINT32 MEM_GetDebugInfo(MEM_INFO *pstMemDebugInfo)
{
    MEM_INFO *pstMemInfo = NULL;
    if (NULL == pstMemDebugInfo)
    {
        CW_SendErr( "[%s,%d] point is NULL ", __FUNCTION__, __LINE__);

        return IPC_STATUS_ARGV;
    }

    if (FALSE == MEM_IsDebugOn())
    {
        return IPC_STATUS_EXCEED;
    }

    if ((pstMemInfo = MEM_DebugInfoInit()) == NULL)
    {
        CW_SendErr( "[%s,%d] get shm error ", __FUNCTION__, __LINE__);

        return IPC_STATUS_ARGV;
    }
    memcpy(pstMemDebugInfo, pstMemInfo, sizeof(MEM_INFO) * MEM_MAX_NUM);

    return IPC_STATUS_OK;
}
void MEM_AddDebugInfo(UINT8 *pBuf, UINT32 uiSize, UINT8 *szFunName, UINT32 uiLine)
{
    static UINT32 static_Index = 0;
    MEM_INFO *pstMemInfo = NULL;
    UINT32 i = 0;

    /* 如果内存调试开关打开，则需要将内存使用信息加入到共享内存中 */
    if (TRUE == MEM_IsDebugOn())
    {
        if ((pstMemInfo = MEM_DebugInfoInit()) == NULL)
        {
            CW_SendErr( "[%s,%d] get shm error ", __FUNCTION__, __LINE__);

            return;
        }

        for(i = 0; i < MEM_MAX_NUM; i++)
        {
            if(TRUE == pstMemInfo[i].ucValid)
            {
                continue;
            }

            pstMemInfo[i].ucValid = TRUE;
            pstMemInfo[i].uiLine = uiLine;
            pstMemInfo[i].uiIndex = static_Index;
            static_Index++;
            strncpy(pstMemInfo[i].szFunName, szFunName, sizeof(pstMemInfo[i].szFunName));
            pstMemInfo[i].szFunName[sizeof(pstMemInfo[i].szFunName)-1] = 0;
            pstMemInfo[i].uiSize = uiSize;
            pstMemInfo[i].pBuf = pBuf;
            pstMemInfo[i].uiPid = getpid();

            char buf[512] = {0};

            sprintf(buf, "malloc: [%32s:%d] index=%d, size=%d, data=0x%x, pid=%d\r\n",
                    pstMemInfo[i].szFunName, pstMemInfo[i].uiLine,
                    pstMemInfo[i].uiIndex, pstMemInfo[i].uiSize,
                    (UINT)pstMemInfo[i].pBuf, pstMemInfo[i].uiPid);
            MEM_SaveToFile(buf);
            break;
        }

        if (i == MEM_MAX_NUM)
        {
            CW_SendErr(  "[%s,%d] mem debug is full \r\n", __FUNCTION__, __LINE__);
        }
    }

}

UINT8 *MEM_Strdup(UINT8 *pucStr, UINT8 *szFunName, UINT32 uiLine)
{
    UINT8 *pBuf = NULL;
    pBuf = strdup(pucStr);
    if(NULL == pBuf)
    {
        return NULL;
    }

    MEM_AddDebugInfo(pBuf, strlen(pBuf), szFunName, uiLine);

    return pBuf;
}

UINT8 *MEM_Calloc(size_t sNum, UINT32 uiSize, UINT8 *szFunName, UINT32 uiLine)
{
    UINT8 *pBuf = NULL;

    pBuf = calloc(sNum, uiSize);
    if(NULL == pBuf)
    {
        return NULL;
    }

    MEM_AddDebugInfo(pBuf, uiSize, szFunName, uiLine);

    return pBuf;
}

UINT8 *MEM_Realloc(void * ptr, UINT32 uiSize, UINT8 *szFunName, UINT32 uiLine)
{
    UINT8 *pBuf = NULL;

    pBuf = realloc(ptr, uiSize);
    if(NULL == pBuf)
    {
        return NULL;
    }
    MEM_DelDebugInfo(ptr);

    MEM_AddDebugInfo(pBuf, uiSize, szFunName, uiLine);

    return pBuf;
}

UINT8 *MEM_Malloc(UINT32 uiSize, UINT8 *szFunName, UINT32 uiLine)
{
    UINT8 *pBuf = NULL;

    pBuf = malloc(uiSize);
    if(NULL == pBuf)
    {
        return NULL;
    }

    MEM_AddDebugInfo(pBuf, uiSize, szFunName, uiLine);

    return pBuf;
}
void MEM_DelDebugInfo(UINT8 *pBuf)
{
    UINT32 i = 0;
    MEM_INFO *pstMemInfo = NULL;

    /* 如果内存调试开关打开，则需要将内存使用信息从共享内存中删除 */
    if (TRUE == MEM_IsDebugOn())
    {
        if ((pstMemInfo = MEM_DebugInfoInit()) == NULL)
        {
            CW_SendErr(  "[%s,%d] get shm error ", __FUNCTION__, __LINE__);
        }
        else
        {
            for(i = 0; i < MEM_MAX_NUM; i++)
            {
                if(TRUE != pstMemInfo[i].ucValid)
                {
                    continue;
                }
                if (pBuf == (UINT8*)pstMemInfo[i].pBuf)
                {
                    pstMemInfo[i].ucValid = FALSE;

                    char buf[512] = {0};

                    sprintf(buf, "free: [%32s:%d] index=%d, size=%d, data=0x%x, pid=%d\r\n",
                            pstMemInfo[i].szFunName, pstMemInfo[i].uiLine,
                            pstMemInfo[i].uiIndex, pstMemInfo[i].uiSize,
                            (UINT)pstMemInfo[i].pBuf, pstMemInfo[i].uiPid);
                    MEM_SaveToFile(buf);
                    break;
                }
            }
        }
    }
}
void MEM_Free(UINT8 *pBuf)
{
    if(NULL == pBuf)
    {
        return;
    }
    MEM_DelDebugInfo(pBuf);
    free(pBuf);
    return;
}

INT32 MEM_FreeCache(void)
{
    struct timeval stNowTime;
    static INT32  cvn_shuck_iStartTimeuse=0;
    INT32  iNowTimeuse=0;
    INT32 ret = 0;
    gettimeofday(&stNowTime, NULL);
    iNowTimeuse = stNowTime.tv_sec;

    if(((iNowTimeuse - cvn_shuck_iStartTimeuse ) >= 5) ||
        (iNowTimeuse < cvn_shuck_iStartTimeuse))
    {
        gettimeofday(&stNowTime, NULL);
        cvn_shuck_iStartTimeuse = stNowTime.tv_sec;
        ret = malloc_trim(0);

        system("sync");
        system("echo 3 > /proc/sys/vm/drop_caches");
        system("echo 0 > /proc/sys/vm/drop_caches");

        CW_SendDebug("free cache success, trim=%d\n", ret);

        return IPC_STATUS_OK;
    }
    else
    {
        return IPC_STATUS_OK;
    }
}


INT32 MEM_SaveToFile(char *szBuf)
{
    #if 0
    INT32 ret = 0;
    FILE *fp = NULL;
    char tmp[100];

    fp = fopen("/tmp/mem_log.txt", "a+");
    if (NULL == fp)
    {
        CW_SendErr(  "file open Fail!\n");
        return -1;
    }

    fwrite(szBuf, 1, strlen(szBuf), fp);
    fclose(fp);
    fp = NULL;
    #endif
    return 0;
}



