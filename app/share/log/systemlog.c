/*
 * syslog.c
 *
 *  Created on: 2013/09/06
 *      Author: root
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include <errno.h>
#include <signal.h>
#include <syslog.h>
#include <netdb.h>
#include <time.h>
#include <sys/types.h>
#include <sys/param.h>
#include <sys/socket.h>
#include <sys/un.h>
#include <sys/ioctl.h>
#include <net/if.h>
#include <netinet/in_systm.h>
#include <netinet/in.h>
#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/tcp.h>
#include <netinet/if_ether.h>
#include <linux/if_packet.h>
#include <linux/stddef.h>
#include <stddef.h>
#include <sys/time.h>

#include "private_type.h"
#include "private_log.h"

#include "ipc_protocol.h"



/*==================================================================
* Function	: LogIsNeedPrint
* Description	: 检查是否需要打印
* Input Para	:
* Output Para	:
* Return Value:
==================================================================*/
UINT32 LogIsNeedPrint(int Level)
{
    UINT32 *puiDebugModule = NULL;
    /* 非debug打印不做限制 */
    if(LOG_DEBUG != (Level&0x7))
    {
        return TRUE;
    }

    if((access(DBG_DEBUG_ON, F_OK)) != -1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
    return TRUE;
}
UINT32 DumpLogIsNeedPrint()
{
    if((access(DBG_DUMP_ON, F_OK)) != -1)
    {
        return TRUE;
    }
    else
    {
        return FALSE;
    }
}
void sys_vsyslog(int priority, const char *fmt, va_list ap)
{
    #define LIBU_WIN_LOGFILE "courage.log"
    enum { BUFSZ = 1024 };
    static FILE *df = NULL;
    char buf[BUFSZ];
    char file[256];
    int i;

    sprintf(file, "%s.%d", LIBU_WIN_LOGFILE,getpid());
    /* first time open the log file and the lock file */
    if(df == NULL)
    {
        df = fopen(file, "a+");
        if(df == NULL )
        {
            return;
        }
    }

    vsnprintf(buf, BUFSZ, fmt, ap);

    fprintf(df, "%s\n", buf);
    fflush(df);

    return;
}

void sys_syslog(int priority, const char *fmt, ...)
{
    /* 检测是否需要打印 */
    if(FALSE == LogIsNeedPrint(priority))
    {
        return;
    }
    va_list ap;

    va_start(ap, fmt); /* init variable list arguments */

    sys_vsyslog(priority, fmt, ap);

    va_end(ap);
}

#define SEC_USEC (1000 * 1000)
#define SEC_MSEC (1000)

UINT32 sys_GetTimeMsecs()
{
    struct timeval tv;
    struct timezone tz;
    UINT32 uiUsec;

    gettimeofday(&tv, &tz);
    uiUsec = (UINT32)(tv.tv_sec * SEC_MSEC + tv.tv_usec/1000);

    return uiUsec;
}

UINT32 sys_GetTimeUsecs()
{
    struct timeval tv;
    struct timezone tz;
    UINT32 uiUsec;

    gettimeofday(&tv, &tz);
    uiUsec = (UINT32)(tv.tv_sec * SEC_USEC + tv.tv_usec);

    return uiUsec;
}
UINT32 sys_GetTimeSec()
{
    struct timeval tv;
    struct timezone tz;
    UINT32 uiUsec;

    gettimeofday(&tv, &tz);
    uiUsec = (UINT32)(tv.tv_sec);

    return uiUsec;
}

char * sys_GetTimeStr()
{
    struct tm *ptr;
    time_t it;
    static char szTime[BUF_SIZE64] = {0};

    it=time(NULL);

    ptr=localtime(&it);
    sprintf(szTime, "%4d-%02d-%02d %d:%d:%d",
            ptr->tm_year+1900,ptr->tm_mon+1,ptr->tm_mday,ptr->tm_hour,ptr->tm_min,ptr->tm_sec);

    return szTime;
}

